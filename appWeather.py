import requests
import pymongo
import json
import datetime
from flask import Flask, request
from flask_restplus import Api, swagger, Resource, Namespace
app = Flask(__name__)
api = Api(app, version='1.3', title="Weather Changes", description="Shows weather changes",
          contact_email='skmaifuj.alam@gslab.com')

def weather_request():
    url = 'http://api.openweathermap.org/data/2.5/weather?q=Pune&appid=bed5975bf04e5540769c66d9af9e3873&units=metric'
    res = requests.get(url)
    data = res.json()
    return data  # Weather request result in dictionary


def insert(data):
    print("Writing in DB")
    client = pymongo.MongoClient('localhost', 27017)
    data.update({'time': getTimeStamp()})
    x2 = client['weather']['c1'].insert_one(data)
    client.close()
    print(x2.acknowledged)
    return x2.acknowledged


def getTimeStamp():
    hr = datetime.datetime.now().hour
    #mn = datetime.datetime.now().minute
    d1 = datetime.datetime.now().day
    mo = datetime.datetime.now().month
    now_time = {'month': mo, 'day': d1, 'hour': hr}
    return now_time


def dbCheck(timestamp):
    client = pymongo.MongoClient('localhost', 27017)
    search_result=client['weather']['c1'].find({'time.hour':timestamp.get('hour')}, {'_id': False})  # Doing DB search
    client.close()
    for i in search_result:
        return i
    return {}  #If not found anything return empty Dict

@api.route('/get_weather_now/')
class GetAll(Resource):
    def get(self):
        now=getTimeStamp()
        xx=dbCheck(now)   # Checking in DB.If found in DB then return its all data
        if xx != {}:  # Found in db no need to request API
            print("Found in DB")
            return xx
        else:  # Request API and write in db
            print("Not Found IN DB")
            requesting = weather_request()    # Requesting API
            insert(requesting)  # Storing result(which got from API Call) in DB as desire result is not found in db
            return dbCheck(now)


if __name__ == '__main__':
    app.run(debug=True)
